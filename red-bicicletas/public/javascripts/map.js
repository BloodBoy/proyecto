var map = L.map("main_map").setView(
  [17.995714045907462, -94.57170684514347],
  13
);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
  attribution: '&copy <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}
).addTo(map);

$.ajax({
  dataType: "json",
  url: "/api/mototaxis",
  success: function (result) {
    console.log(result);
    result.mototaxis.forEach(function (moto) {
      L.marker(moto.ubicacion, { title: moto.id }).addTo(map);
    });
  },
});
// L.marker([18.135194243356104, -94.46020108903387],{title:'mamada'}).addTo(map);
