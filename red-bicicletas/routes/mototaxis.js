var express = require('express');
var router = express.Router();
var motoController = require('../controllers/mototaxi');

router.get('/',motoController.mototaxi_list);
router.get('/create',motoController.mototaxi_create_get);
router.post('/create',motoController.mototaxi_create_post);
router.post('/:id/delete',motoController.mototaxi_delete_post);
router.get("/:id/update", motoController.mototaxi_update_get);
router.post("/:id/update", motoController.mototaxi_update_post);

module.exports = router;