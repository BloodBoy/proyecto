var express = require('express');
var router = express.Router();
var motoController = require('../../controllers/api/mototaxiControllerApi')

router.get('/',motoController.mototaxi_list);
router.post('/create',motoController.mototaxi_create);
router.delete('/delete',motoController.mototaxi_delete);
module.exports = router;