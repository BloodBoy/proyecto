var mongoose = require("mongoose");
var schema = mongoose.Schema;

var mototaxiSchema = new schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: {
      type: "2dsphere",
      sparse: true,
    },
  },
});

mototaxiSchema.statics.createInstance = function(code,color,modelo,ubicacion){
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion

  });
};

mototaxiSchema.methods.toString = function(){
  return 'code: ' + this.code + ' | color: ' + this.color;
};

mototaxiSchema.statics.allMotos = function(cb){
  return this.find({},cb);
};

module.exports = mongoose.model('Mototaxi',mototaxiSchema);
