var mongoose = require("mongoose");
var Reserva = require("./reserva");
var Schema = mongoose.Schema;

let usuarioSchema = new Schema({
  name: String ,
});

usuarioSchema.methods.reservar = function (motoId, desde, hasta, cb) {
  let reserva = new Reserva({
    usuario: this.id,
    mototaxi: motoId,
    desde: desde,
    hasta: hasta,
  });
  reserva.save(cb);
};

module.exports = mongoose.model("Usuario", usuarioSchema);
