var Mototaxi = require('../../models/mototaxi')

exports.mototaxi_list = function (req, res) {
    res.status(200).json({
        mototaxis: Mototaxi.allMotos
    })
}
exports.mototaxi_create = function (req, res) {
    var moto = new Mototaxi(req.body.id, req.body.color, req.body.modelo);
    moto.ubicacion = [req.body.lat, req.body.lng];

    Mototaxi.add(moto);
    res.status(200).json({
        mototaxi: moto
    })
}

exports.mototaxi_delete = function (req, res) {
    Mototaxi.removeById(req.body.id);
    res.status(204).send();
}