var Mototaxi = require('../models/mototaxi');

exports.mototaxi_list = function (req, res){
  res.render('mototaxis/index',{motos: Mototaxi.allMotos});
}

exports.mototaxi_create_get = function (req, res){
  res.render('mototaxis/create')
}

exports.mototaxi_create_post = function (req,res){
  var mototaxi = new Mototaxi(req.body.id,req.body.color,req.body.modelo);
  mototaxi.ubicacion = [req.body.lat,req.body.lng];
  Mototaxi.add(mototaxi);
  res.redirect('/mototaxis')
}

exports.mototaxi_update_get = function (req, res) {
  var mototaxi = Mototaxi.findById(req.params.id);
  res.render('mototaxis/update', {mototaxi});
};

exports.mototaxi_update_post = function (req, res) {
  var mototaxi = Mototaxi.findById(req.params.id);
  mototaxi.id = req.body.id;
  mototaxi.color = req.body.color;
  mototaxi.modelo = req.body.modelo;
  mototaxi.ubicacion = [req.body.lat, req.body.lng];
  res.redirect("/mototaxis");
};

exports.mototaxi_delete_post = function (req, res){
  Mototaxi.removeById(req.body.id);
  res.redirect('/mototaxis');
}