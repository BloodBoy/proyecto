let Mototaxi = require("../../models/mototaxi");
let req = require("request");
let server = require("../../bin/www");


beforeEach(() => {
  Mototaxi.allMotos = [];
});

describe("Mototaxi API", () => {
  describe("GET MOTOTAXI/", () => {
    it("Status 200", () => {
      expect(Mototaxi.allMotos.length).toBe(0);

      let a = new Mototaxi(1, "Azul", "lujo", [-30.45, -50.34]);
      Mototaxi.add(a);
      req.get(
        "http://localhost:3000/api/mototaxis",
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
        }
      );
    });
  });

  describe("POST MOTOTAXI/", () => {
    it("Status 200", (done) => {
      let headers = '{ "content-type": "application/json" }';
      req.post(
        "http://localhost:3000/api/mototaxis/create",
        {
          json: {
            id: 10,
            color: "rojo",
            modelo: "lujo",
            lat: 18.135194243356104,
            lng: -94.46020108903387,
          },
        },
        (error, response, body) => {
          expect(response.statusCode).toBe(200);
          expect(Mototaxi.findById(10).color).toBe("rojo");
          done();
        }
      );
    });
  });
});
