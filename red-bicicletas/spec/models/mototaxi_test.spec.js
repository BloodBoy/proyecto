var mongoose = require("mongoose");
var Mototaxi = require("../../models/mototaxi");

describe("Testing Mototaxi", () => {
  beforeEach((done) => {
    let mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("We are connected to database");
      done();
    });
  });

  afterEach(function (done) {
    Mototaxi.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      mongoose.disconnect(err);
      done();
    });
  });

  describe("Mototaxi.createInstance", () => {
    it("crea una instancia de motos", (done) => {
      let moto = Mototaxi.createInstance(1, "Verde", "Urbano", [-30.4, -20.4]);
      expect(moto.code).toBe(1);
      expect(moto.color).toBe("Verde");
      expect(moto.modelo).toBe("Urbano");
      done();
    });
  });

  describe("Mototaxi.allBicis", () => {
    it("comienza vacia", (done) => {
      Mototaxi.allMotos(function (err, motos) {
        expect(motos.length).toBe(0);
        done();
      });
    });
  });

  describe("Mototaxi.add", () => {
    it("Agrega solo una moto", (done) => {
      let aMoto = new Mototaxi({ code: 10, color: "Azul", modelo: "Tuneado" });
      Mototaxi.add(aMoto, (err, newMoto) => {
        if (err) console.log(err);
        Mototaxi.allMotos(function (err, motos) {
          expect(motos.length).toEqual(1);
          expect(motos[0].code).toEqual(aMoto.code);
          done();
        });
      });
    });
  });

  describe("Mototaxi.findByCode", () => {
    it("debe devolver los valores del code 1", (done) => {
      Mototaxi.allMotos((err, motos) => {
        expect(motos.length).toBe(0);
        let aMoto = new Mototaxi({
          code: 1,
          color: "Azul",
          modelo: "Tuneado",
        });
        Mototaxi.add(aMoto, function (err, newMoto) {
          if (err) console.log(err);
          Mototaxi.findByCode(1, function (err, targetMoto) {
            expect(targetMoto.code).toBe(aMoto.code);
            expect(targetMoto.color).toBe(aMoto.color);
            expect(targetMoto.modelo).toBe(aMoto.modelo);
            done();
          });
        });
      });
    });
  });
});

/* 
beforeEach(() => {
  Mototaxi.allMotos = [];
});

describe("Mototaxi.allMotos", () => {
  it("comienza vacio", () => {
    expect(Mototaxi.allMotos.length).toBe(0);
  });
});

describe("Mototaxi.add", () => {
  it("agregando una", () => {
    expect(Mototaxi.allMotos.length).toBe(0);
    let a = new Mototaxi(1, "rojo", "tuneado", [
      18.07563575667307,
      -94.65063559116241,
    ]);
    Mototaxi.add(a);
    expect(Mototaxi.allMotos.length).toBe(1);
    expect(Mototaxi.allMotos[0]).toBe(a);
  });
});

describe("Mototaxi.findById", () => {
  it("Debe devolver la bici con ID 1", () => {
    expect(Mototaxi.allMotos.length).toBe(0);
    let aMoto = new Mototaxi(1, "rojo", "tuneado", [
      18.07563575667307,
      -94.65063559116241,
    ]);
    Mototaxi.add(aMoto);
    let targetID = Mototaxi.findById(1);
    expect(targetID.id).toBe(1);
    expect(targetID.color).toBe(aMoto.color);
    expect(targetID.modelo).toBe(aMoto.modelo);
  });
});

describe("Mototaxi.removeById", () => {
  it("Debe eliminar un elemento por su ID", () => {
    expect(Mototaxi.allMotos.length).toBe(0);
    let aMoto = new Mototaxi(1, "rojo", "tuneado", [
      18.07563575667307,
      -94.65063559116241,
    ]);
    Mototaxi.add(aMoto);
    expect(Mototaxi.allMotos.length).toBe(1);
    Mototaxi.removeById(1);
    expect(Mototaxi.allMotos.length).toBe(0);
  });
});
 */
