var mongoose = require("mongoose");
let Mototaxi = require("../../models/mototaxi");
let Usuario = require("../../models/usuario");
let Reserva = require("../../models/reserva")

describe("Testing usuarios",()=>{
    beforeEach(function(done){
       let mongoDB ='mongodb://localhost/testdb';
       mongoose.connect(mongoDB,{useNewUrlParser: true});

       const db = mongoose.connection;
       db.on('error',console.error.bind(console,"connection error"));
       db.once('open',() =>{
           console.log("We are connected to the database");
           done();
       } );
    });
    afterEach(function(done){
        Reserva.deleteMany({},function(err,success){
           if(err)console.log(err);
           Usuario.deleteMany({},function(err,success){
               if(err) console.log(err);
               Mototaxi.deleteMany({},function(err,success){
                  if(err) console.log(err);
                  done();
               });
           })
        });
    });
    describe("Cuando un usuario reseva una moto", function(){
        it("Desde exisir la reserva",(done)=>{
            const  usuario = new Usuario({name:"Alan"});
            usuario.save();
            const  mototaxi = new Mototaxi({code: 1,color:"Verde",modelo:"Urbana"});
            mototaxi.save();
            let hoy = new Date();
            let tomorrow = new Date();
            tomorrow.setDate(hoy.getDate()+1);
            usuario.reservar(mototaxi.id,hoy,tomorrow,function(err,reserva){
                Reserva.find({}).populate('motototaxi').exec(function(err,reservas){
                    console.log(reservas[0]);
/*                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].mototaxi.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.name);*/
                    done();
                });
            });
        });
    });
});